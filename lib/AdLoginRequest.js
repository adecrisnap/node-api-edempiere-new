const BaseClass = require('./baseclass,js')

class ADLoginRequestClass extends BaseClass {
    constructor(name, user, password) {
        super(name);
        this.user = user;
        this.password = password
    } 
    
    lang = "en_US";
    ClientID = "1000003";
    RoleID = "1000006";
    OrgID = "0";
    WarehouseID = "0";
    stage = "9"    
}

module.exports = ADLoginRequestClass