class ReadWrite {
    constructor() {
        console.log('Read Write'); 
        this.obj;   
    }
    static makeObject() {        
        if (!this.obj) {
            this.obj = new ReadWrite();
        }
        return this.obj;
    }
    read(){
        return "read"
    }

    write(){
        return "write"
    }


    async readWrite() {
        try {
            const obj = ReadWrite.makeObject();
            const result = await Promise.all([ obj.read(), obj.write()])
            console.log(result);
            return result
        }
        catch(err) {
            console.log(err);
            
        }
    }
}
module.exports = ReadWrite;