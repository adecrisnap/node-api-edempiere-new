
const ReaderClass = require("../ReaderClass");

class SalesRepresentativeReaderClass extends ReaderClass {
    constructor() {
        super();
    }

    async getSalesRegion(){
        var data = JSON.stringify({
            "ModelCRUDRequest": {
              "ModelCRUD": {
                "serviceType": "getSalesRegion",
                "TableName": "C_BPartner",
                "Action": "Read"
              },
              "ADLoginRequest" : this.ADLoginRequest
            }
          });

        try {
            const response = await this.axios({
                method: 'post',
                data: data
            });

            if (response.status==200) {
              var arr = []
              var data = response.data.WindowTabData.DataSet.DataRow
              if (response.data.WindowTabData.RowCount==1) {
                  arr.push({
                      key : data.field[0].val, 
                      id : data.field[0].val, 
                      partner_id : data.field[0].val,
                      name : data.field[1].val, 
                      value : data.field[1].val, 
                      text :  data.field[1].val,
                  })
              }
              else {
                  for (var i=0;i<data.length;i++) {
                      arr.push({
                          key : data[i].field[0].val,
                          id :  data[i].field[0].val,
                          partner_id : data[i].field[0].val,
                          name : data[i].field[1].val,
                          value :  data[i].field[1].val,
                          text : data[i].field[1].val,   
                      })
                  }
              }
              return arr;
          }
        } catch (err) {
            console.error(err.message);
        }   
    }

}

module.exports = SalesRepresentativeReaderClass;