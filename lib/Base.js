var axios = require('axios');
const username = "belitangAdmin";
const password = "belitangAdmin";

 class BaseClass {
    constructor() {
        const basedUrl = 'http://108.137.95.190:8080/ADInterface/services/rest';
        const basedReadURL = 'http://108.137.95.190:8080/ADInterface/services/rest/model_adservice/query_data';
        const basedWriteURL = 'http://108.137.95.190:8080/ADInterface/services/rest/composite_service/composite_operation';

        this.axios = axios.create({
            baseURL: basedReadURL,
            timeout: 5000,
            headers: { 
                'Accept': 'application/json', 
                'Content-Type': 'application/json'
            }
          });
        
        this.ADLoginRequest = {
            "user": username,
            "pass": password,
            "lang": "en_US",
            "ClientID": "1000003",
            "RoleID": "1000006",
            "OrgID": "0",
            "WarehouseID": "0",
            "stage": "9"
        }
    }
}

module.exports = BaseClass;