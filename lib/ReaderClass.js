const BaseClass = require("./Base");
const username = "belitangAdmin";
const password = "belitangAdmin";


class ReaderClass extends BaseClass {
    constructor() {
        super();
        const basedReadURL = '/model_adservice/query_data';

        this.axios.url = basedReadURL;

        this.ADLoginRequest = {
            "user": username,
            "pass": password,
            "lang": "en_US",
            "ClientID": "1000003",
            "RoleID": "1000006",
            "OrgID": "0",
            "WarehouseID": "0",
            "stage": "9"
        }
    }


}


module.exports = ReaderClass;