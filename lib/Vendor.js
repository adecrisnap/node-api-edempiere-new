
const BaseClass = require("./Base");

class VendorClass extends BaseClass {
    constructor() {
        super();
    }

    async getVendor(){
        var data = JSON.stringify({
            "ModelCRUDRequest": {
              "ModelCRUD": {
                "serviceType": "getVendors",
                "TableName": "C_BPartner",
                "Action": "Read"
              },
              "ADLoginRequest" : this.ADLoginRequest
            }
          });

        try {
            const response = await this.axios({
                method: 'post',
                data: data
            });

            if (response.status==200) {
              var arr = []
              var data = response.data.WindowTabData.DataSet.DataRow
              if (response.data.WindowTabData.RowCount==1) {
                  arr.push({
                      partner_id : data.field[0].val,
                      value : data.field[1].val,
                      name : data.field[2].val 
                  })
              }
              else {
                  for (var i=0;i<data.length;i++) {
                      arr.push({
                          partner_id : data[i].field[0].val,
                          value : data[i].field[1].val,
                          name : data[i].field[2].val 
                      })
                  }
              }
              return arr;
          }
        } catch (err) {
            console.error(err.message);
        }   
        
          
    }
   
}
module.exports = VendorClass;