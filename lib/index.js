const axios = require('axios');

module.exports = class OpenDota {
  constructor(apiKey) {
    axios.defaults.baseURL = 'https://api.opendota.com/api';
    axios.defaults.headers.common['Authorization'] = `Bearer ${apiKey}`;
    //axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  async proMatches() {
    const { data } = await axios.get('/proMatches');
    return data;
  }

  async match(matchId) {
    const { data } = await axios.get('/matches/${matchId}');
    return data;
  }

  async proPlayers() {
    const { data } = await axios.get('/proPlayers');
    return data;
  }

  async player(accountId) {
    const { data } = await axios.get('/players/${accountId}');
    return data;
  }
};