const express = require('express')
const router = express.Router()

const Vendor = require('../lib/Vendor');
const SalesRepresentative = require('../lib/SalesRep/SalesRepresentativeReader');


class Start1 {
    constructor() {
    }
  
    async getVendor(req, res, next) {
      const vendor = new Vendor();
      const result = await vendor.getVendor();
      res.send(result);
    }

    async getSalesRegion(req, res, next) {
        const salesRepresentative = new SalesRepresentative();
        const result = await salesRepresentative.getSalesRegion();
        res.send(result);
    }
}

const obj1 = new Start1();

router.get('/vendor', obj1.getVendor);

router.get('/salesman-region', obj1.getSalesRegion);

router.post('/salesman', obj1.getSalesRegion);

module.exports = router;