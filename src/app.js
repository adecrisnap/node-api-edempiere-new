//const ReadWrite = require('../lib/ReadWrite').makeObject();
var express = require('express');
var cors = require('cors')
const app = express();

app.use(cors())
app.use('/', require('../routes/index'));

//const Vendor = require('../lib/Vendor');


class Start {
  constructor() {
      const server = app.listen(9002),
      host = server.address().address,
      port = server.address().port
      console.log("Example app listening at http://%s:%s", host, port)
  }

  // async getVendor(req, res, next) {
  //   const vendor = new Vendor();
  //   const result = await vendor.getVendor();
  //   res.send(result);
  // }
}

const obj1 = new Start();
//app.get('/', obj1.route);
//app.get('/vendor', obj1.getVendor);